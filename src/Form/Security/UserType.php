<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 13.01.2018
 * Time: 19:17
 */

namespace App\Form\Security;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, array('label' => 'Imię'))
            ->add('fullsurname', TextType::class, array('label' => 'Nazwisko'))
            ->add('email', EmailType::class, array('label' => 'Adres e-mail'))
            ->add('peselnumber', TextType::class, array('label' => 'Numer pesel'))
            ->add('birthdate', TextType::class, array('label' => 'Data urodzenia', 'attr' => array('class' => 'datepicker')))
            ->add('username', TextType::class, array('label' => 'Login'))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Hasło'),
                'second_options' => array('label' => 'Powtórz hasło'),
            ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}