<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Podany adres email został już użyty")
 * @UniqueEntity(fields="username", message="Nazwa użytkownika jest już zajęta")
 */
class User implements UserInterface, \Serializable
{

    const ROLE_USER  = "ROLE_USER";
    const ROLE_ADMIN = "ROLE_ADMIN";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\NotBlank(
     *     message="Adres email nie może być pusty"
     * )
     * @Assert\Email(
     *     message="Niepoprawny format adresu email"
     * )
     * @Assert\Length(
     *     max="255",
     *     maxMessage="Adres email nie możę mieć więcej niż 255 znaków"
     * )
     */
    private $email;



    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(
     *     message="Nazwa użytkownika nie może być pusta"
     * )
     * @Assert\Length(
     *     min="3",
     *     max="20",
     *     minMessage="Nazwa użytkownika musi mieć conajmniej 3 znaki",
     *     maxMessage="Nazwa użytkowniak nie może przekraczać 20 znaków"
     * )
     */
    private $username;

    /**
     * @Assert\NotBlank(
     *     message="Hasło nie może być puste"
     * )
     * @Assert\Length(
     *     min="6",
     *     max="20",
     *     minMessage="Hasło musi zawierać conajmniej 6 znaków",
     *     maxMessage="Hasło nie może przekraczać 20 znaków"
     * )
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(name="fullname", type="string", length=30)
     * @Assert\NotBlank(message="Pole Imię nie może być puste")
     * @Assert\Length(
     *     min=2,
     *     max=50,
     *     minMessage="Imię nie może mieć mniej niż 2 znaki",
     *     maxMessage="Imię nie możę mieć więcej niż 30 znaków"
     * )
     */
    private $fullname;

    /**
     * @ORM\Column(name="fullsurname", type="string", length=50)
     * @Assert\NotBlank(
     *     message="Pole nazwisko nie może być puste"
     * )
     * @Assert\Length(
     *     min=2,
     *     max=50,
     *     minMessage="Nazwisko nie może mieć mniej niż 2 znaki",
     *     maxMessage="Nazwisko nie możę mieć więcej niż 50 znaków"
     * )
     */
    private $fullsurname;

    /**
     * @ORM\Column(name="peselnumber", type="string", length=13)
     * @Assert\NotBlank(
     *     message="Pole numer PESEL nie może być puste"
     * )
     * @Assert\Length(
     *     min="13",
     *     max="13",
     *     exactMessage="Numer PESEL musi mieć dokładnie 13 znaków"
     * )
     *
     *  @Assert\Regex(
     *     pattern="/\d/",
     *     message="Numer PESEL może się składać wyłącznie z liczb"
     * )
     */
    private $peselnumber;

    /**
     * @ORM\Column(name="birthdate", type="datetime")
     * @Assert\NotBlank(
     *     message="Data urodzenia  nie może być pusta"
     * )
     */
    private $birthdate;

    /**
     *
     * @ORM\Column(name="passwordexpiresat", type="datetime", nullable=true)
     * @var
     */
    private $passwordexpiresat;

    /**
     * @ORM\Column(name="accountauthlink", type="string", length=64, nullable=true)
     * @var
     */
    private $accountauthlink;

    /**
     * @ORM\Column(name="dbrole", type="string", length=20)
     */
    private $dbrole;


    public function setBirthdate($birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function getPeselnumber()
    {
        return $this->peselnumber;
    }

    public function setPeselnumber($peselnumber): void
    {
        $this->peselnumber = $peselnumber;
    }

    public function getFullsurname()
    {
        return $this->fullsurname;
    }

    public function setFullsurname($fullsurname): void
    {
        $this->fullsurname = $fullsurname;
    }


    public function getFullname()
    {
        return $this->fullname;
    }

    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setEmail($email)
    {
        $this->email = $email;
    }


    public function getUsername()
    {
        return $this->username;
    }


    public function setUsername($username)
    {
        $this->username = $username;
    }


    public function getPlainPassword()
    {
        return $this->plainPassword;
    }


    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }


    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return null;
    }


    public function getRoles()
    {
        $role = self::getDbrole();
        return array($role);
    }


    public function eraseCredentials()
    {

    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getPasswordexpiresat()
    {
        return $this->passwordexpiresat;
    }

    /**
     * @param mixed $passwordexpiresat
     */
    public function setPasswordexpiresat($passwordexpiresat): void
    {
        $this->passwordexpiresat = $passwordexpiresat;
    }

    /**
     * @return mixed
     */
    public function getAccountauthlink()
    {
        return $this->accountauthlink;
    }

    /**
     * @param mixed $accountauthlink
     */
    public function setAccountauthlink($accountauthlink): void
    {
        $this->accountauthlink = $accountauthlink;
    }

    /**
     * @return mixed
     */
    public function getDbrole()
    {
        return $this->dbrole;
    }

    /**
     * @param mixed $dbrole
     */
    public function setDbrole($dbrole): void
    {
        $this->dbrole = $dbrole;
    }


}