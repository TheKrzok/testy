<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 15.01.2018
 * Time: 02:16
 */

class AdminFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $encoder = new BCryptPasswordEncoder("8");
        $admin->setFullname('Rufi');
        $admin->setFullsurname('Mistrz');
        $admin->setBirthdate(new \DateTime());
        $password = $encoder->encodePassword('admin', 'salt');
        $admin->setPassword($password);
        $admin->setDbrole(User::ROLE_ADMIN);
        $admin->setEmail('admin@system.com');
        $admin->setPeselnumber('0000000000000');
        $admin->setUsername('admin');

        $manager->persist($admin);
        $manager->flush();
    }
}