<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 */
class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index() {
        return $this->redirectToRoute('login');
    }
}