<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class LogoutController
 */
class LogoutController extends Controller
{
    /**
     * @Route("logout", name="logout")
     */
    public function logout(){}

}