<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class UserProfileController extends Controller
{
    /**
     * @Route("/user/profile", name="user_profile")
     */
    public function viewProfile()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $birthdate = date_format($user->getBirthdate(), 'Y-m-d');

        return $this->render("user\profile.html.twig", array('user' => $user, 'birthdate' => $birthdate));
    }
}